#include "ball.h"

Ball::Ball(pos speed, double r, pos start, pos win) : 
	d(speed), loc(start), sf::CircleShape((float) r), radius(r), ws(win) {
	this->setRadius((float)r); // Just in case
	this->setFillColor(sf::Color::White);
}

void Ball::move() {
	if (loc.x <= 0 || loc.x >= (ws.x) - radius) {
		bounce_x();
	}
	if (loc.y <= 0 || loc.y >= (ws.y) - radius) {
		bounce_y();
	}

	loc.x += d.x;
	loc.y += d.y;

	// Sets drawable position to the location
	this->setPosition(loc.x, loc.y);
}
