#include "player.h"
#include <iostream>

Player::Player(pos paddle, pos location, pos speed, pos windowSize) :
	loc(location), d(speed), ws(windowSize), paddle(paddle), score(0) {
	this->setSize({(float) paddle.x, (float) paddle.y});
	this->setPosition({ (float)location.x, (float)location.y });
	this->setFillColor(sf::Color::Cyan);
}

void Player::display() {
	this->setPosition((float)loc.x, (float)loc.y);
}

bool Player::meet(const pos p) {
	double len = paddle.x;
	double h = paddle.y;

	if (p.x >= this->getLoc().x && p.x <= (this->getLoc().x + len)
		&& p.y >= this->getLoc().y && p.y <= (this->getLoc().y + h)) return true;
	
	return false;
}

User::User(pos paddle, pos location, pos speed, pos windowSize) :
	Player(paddle, location, speed, windowSize) {}

void User::move(const sf::Keyboard::Key key) {

	setLocToDisplayObj();
		
	switch (key)
	{
	case sf::Keyboard::Left:
		if (getLocX() >= -getSize().x/2) move_xb();
		break;
	case sf::Keyboard::Right:
		if (getLocX() <= (getWSX() - getSize().x/2)) move_xf();
		break;
	default:
		break;
	}

	//std::cout << "loc.x" << getLoc().x << "," << "loc.y" << getLoc().y << std::endl;
	display();
}

AI::AI(pos paddle, pos location, pos speed, pos windowSize) :
	Player(paddle, location, speed, windowSize) {}

void AI::move(pos b, pos d) {
	setLocToDisplayObj();

	/*
	if (p.y < 0.6*getWSY()) {
		if (p.x > getLoc().x && getLocX()) move_xf();
		else if (p.x < getLoc().x) move_xb();
	}*/

	// Pretty simple algorithm
	// Calculates x-value at predicted point of intersection (if ball's dy is negative)
	// And heads towards that x-value
	// Based on equation (x1,y1) = (x0, y0) + a(dx,dy)
	// However, if ball's dy isn't negative and the ball is in the User's court, then
	// moves the paddle back to the center
	double x1 = this->getWSX() / 2;
	double y1 = this->getLoc().y;
	double a = (y1 - b.y)/(d.y);
	if (d.y < 0 && b.y < 0.5*getWSY()) x1 = b.x + a*d.x;

	if (x1 > this->getLocX()) move_xf();
	else if (x1 < this->getLocX()) move_xb();

	display();
}
