# README #

Welcome to Pong! This is a very simple Pong game based on the Atari game. 
My objectives with this project were:
1. Learn how to make a simple 2D game.
2. Learn how to work with a multimedia C++ framework such as SDL or SFML (Used SFML).

### What is this repository for? ###

A simple game of Pong, made with SFML (Simple and Fast Multimedia Library).
SFML is a tool used for 2D and 3D multimedia display in C++ (3D with OpenGL).
For now, this game is single-player with AI (just like in the old arcades).

Contains c++ .cpp and .h files, some .dll libraries for SFML use, and textfonts, 
as well as an executable.

### How do I get set up? ###

As of now, I have included an executable (.exe) file with the project.
On Windows (as of now) please download the folder called Executable and run
the .exe file. 

That should get the game running up.

For Linux, if you already have SFML installed, please follow this link:
https://www.sfml-dev.org/tutorials/2.0/start-linux.php
(And instead of just compiling or running for main.cpp, please compile for
all the .cpp files in the project. This can be done by:

$ g++ -c *.cpp -I<sfml-install-path>/include

instead of

$ g++ -c main.cpp -I<sfml-install-path>/include)

I am working on adding .so library objects to the project for a quick Linux executable.

In case of issues:
Please check that the path at which your executable is also contains
a) all the .dll files
b) the .ttf file named arialbold.ttf

### Who do I talk to? ###

Ayush Lall
email at: ayushlall@g.ucla.edu