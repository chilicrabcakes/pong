#include "game.h"
#include <iostream>
#include <sstream>

Game::Game(int wx, int wy, pos ballSpeed, pos ballStart, sf::Color ballColor, int ballRad,
	pos paddleSize, pos userStart, pos aiStart, pos userSpeed, pos aiSpeed, std::string t) \
	: window(sf::VideoMode(windowSize.x, windowSize.y), t), windowSize(wx, wy), 
	ball(ballSpeed, ballRad, ballStart, windowSize),
	user(paddleSize,userStart, userSpeed,windowSize), 
	ai(paddleSize,aiStart, aiSpeed, windowSize), title(t), threshold(10)
{
	run();
}

int Game::run() {

	if (loadFont() == 1) {
		std::cerr << "Couldn't load font, check SFML error output" << std::endl;
		return 1;
	}
	bool keyPressEvent = true;
	while (window.isOpen())
	{
		printScore();
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
			user.move(sf::Keyboard::Left);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
			user.move(sf::Keyboard::Right);
		}

		ball.move();
		ai.move(ball.getLoc(), ball.getSpeed());
		interact(ball, user);
		interact(ball, ai);

		checkIfScored();
		displayAllObjects();
		difficultySettings();
	}

	//std::cout << "SCORE: User-" << user.getScore() << " AI-" << ai.getScore() << std::endl;
	return 0;
}

int Game::interact(Ball& b, Player& p) {
	// The ball is a standard circle, without 'tilt' (i.e. it's axis is parallel to the y-axis)
	// This means that the ball has a y-maxima and a y-minima with tangents perpendicular
	// to the x-axis
	// For example, the unit circle (x^2 + y^2 = 1) has y-maxima at (0,1) and y-minima at (0,-1)
	// Thus, either the minima or the maxima will be the first point to 'touch' the paddle
	// By sf::CircularShape, we are given the ball's top left corner
	// Thus, Maxima: x0 + r, y0, Minima: x0, y0 + 2r
	
	pos max((double)b.getPosition().x + b.getRadius(), ((double)b.getPosition().y));
	pos min((double)b.getPosition().x, ((double)b.getPosition().y) + 2*b.getRadius());
	if (p.meet(max) || p.meet(min)) {
		b.bounce_y();
	}

	return 0;
}

void Game::checkIfScored() {
	if (ball.getLoc().y <= 0) {
		user.addToScore();
	}
	else if (ball.getLoc().y >= (windowSize.y - ball.getRadius()) ) {
		ai.addToScore();
	}
}

int Game::loadFont() {
	if (!font.loadFromFile("arialbold.ttf")) return 1;
	score.setFont(font);
	return 0;
}

void Game::printScore() {
	std::stringstream s;
	s << "Player: " << user.getScore() << " Computer: " << ai.getScore();

	score.setString(s.str());
	score.setCharacterSize(20);
	score.setFillColor(sf::Color::White);
	score.setPosition({ 0 , (float)(windowSize.y - 40) });
}

void Game::displayAllObjects() {
	window.clear();
	window.draw(ball);
	window.draw(user);
	window.draw(ai);
	window.draw(score);
	window.display();
}

void Game::difficultySettings() {
	if (user.getScore() - ai.getScore() > threshold) {
		ai.increaseSpeed();
		threshold += 10;
	}
}