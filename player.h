/////////////////////////////////////////////////////////////////////////////
// PLAYER CLASS
// Contains the player object, and it's two derived classes: User and AI
// Contains move, draw and getter setters.
// The Player class, just like the ball class, is derived from an sfml shape
// object, specifically the sf::RectangleShape.
// Note: The pure virtual function move has parameter sf::Keyboard::Key, which
// is an enum of keyboard keys in the sf::Event union object
/////////////////////////////////////////////////////////////////////////////

#include "ball.h"

class Player : public sf::RectangleShape {
public:
	Player(pos paddle = { 50,5 }, pos location = { 540, 50 }, pos speed = { 0.2,0.2 },
		pos windowSize = { 640,480 });

	// Move commands: Moves paddle forward or backward
	inline void move_xf() { loc.x += d.x; }
	inline void move_xb() { loc.x -= d.x; }

	// Getters and Setters
	inline double getLocX() const { return loc.x; }
	inline double getSpeedX() const { return d.x; }
	inline double getWSX() const { return ws.x; }
	inline double getWSY() const { return ws.y; }
	inline pos getLoc() const { return loc; }
	inline pos getSize() const { return paddle; }
	inline int getScore() const { return score; }

	inline void addToScore() { score++; }

	// Calculates if the given point is within the bounds of the Player's paddle
	bool meet(const pos p);

	// Difficulty multiplier
	inline void increaseSpeed(double inc = 0.01) { d += 0.01; }
protected:
	// Setter
	inline void setLocToDisplayObj() {
		loc.x = (double) this->getPosition().x;
		loc.y = (double) this->getPosition().y;
	}

	// Adds the location to the sf::RectangleShape object
	void display();
private:
	// Window dimensions
	pos ws;
	// Current location
	pos loc;
	// Speed of movement
	pos d;
	// Size of paddle
	pos paddle;
	// Score!
	int score;
};

class User : public Player {
public:
	User(pos paddle = { 50,5 }, pos location = { 540, 50 }, pos speed = { 0.2,0.2 },
		pos windowSize = { 640,480 });

	// Takes sf::Keyboard::Key type that returns which key was pressed
	// If the key is left, then move_xb is called, and move_xf is called
	// if right is pressed
	void move(const sf::Keyboard::Key key);
};

class AI : public Player {
public:
	AI(pos paddle = { 50,5 }, pos location = { 540, 50 }, pos speed = { 0.2,0.2 },
		pos windowSize = { 640,480 });

	// Moves AI paddle
	// Algorithm: Get as close to the x-position of the ball as possible
	void move(pos p, pos d);
};
