#include "misc.h"

/////////////////////////////////////////////////////////////////////////////
// BALL CLASS
// Contains the class for the moving ball in the Pong game
// Derived from SFML class sf::CircleShape
////////////////////////////////////////////////////////////////////////////

class Ball : public sf::CircleShape {
	public:
		Ball(pos speed = { 0.01,0.01 }, double radius = 50, pos start = { 320,240 }, 
			pos windowSize = { 640,480 });

		// Function that moves the ball around upon call
		void move();

		// The bounce functions. Change the direction of the dx,dy vector - to be called 
		// if a side is hit, or a paddle is hit, etc.
		inline void bounce_x() { d.x *= -1; }
		inline void bounce_y() { d.y *= -1; }

		// Getters and setters 
		inline pos getLoc() const { return loc; }
		inline void setLoc(pos p) { loc = p; }
		inline pos getSpeed() const { return d; }
		inline double getRadius() const { return radius; }

	private:
		// Current location
		pos loc;
		// Speed of movement (dx and dy)
		pos d;
		// Radius
		double radius;
		// Size of the window (rather store it than have it as a global const)
		pos ws;
	};

