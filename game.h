//////////////////////////////////////////////////////////////////////////////
// MAIN GAME CLASS
// Contains all the main functions, instances of player and ball objects, etc.
// A little note about the window: The sf::RenderWindow object opens up a window
// of the size specified in the video mode parameter object. This is aligned in such
// a way that the top-left corner is the value (0,0) and the bottom-left corner
// is the value (window_x, window_y). 
/////////////////////////////////////////////////////////////////////////////
#include "player.h"
#include <string>

class Game {
public:
	// Game class constructor
	Game(int window_x, int window_y, pos ballSpeed, pos ballStart, sf::Color ballColor, int ballRad,
		pos paddleSize, pos userStart, pos aiStart, pos userSpeed, pos aiSpeed, std::string title);

	// Main run function (this is looping every frame)
	// Contains the main action - two while loops, one that is the frame-by-frame event, and
	// the other which collects events per frame
	// Events: key presses, mouse presses, etc. - user input
	// Frame: One single still of the game. A game is just a moving set of frames
	// with the computer changing something each frame depending on user input
	int run();

	// Checks the ball's position with the Player object(s). 
	// If the ball touches any of the objects, switches direction (i.e. bounces)
	int interact(Ball& b, Player& p);

	// Checks if the ball hits the y-bounds, if 0 then score to user, if y-max then score to ai
	void checkIfScored();

	// Sets up the sf::Font object; loads the Font
	int loadFont();

	// Prints the score!
	void printScore();

	// Displays all the objects in the main run loop
	void displayAllObjects();

	// Makes the game progressively more and more difficult
	void difficultySettings();

private:
	pos windowSize;
	Ball ball;
	User user;
	AI ai;

	//Difficulty
	int threshold;

	// The title on the top bar!
	std::string title;

	// For printing out the score, font and text
	sf::Font font;
	sf::Text score;

	// Window
	sf::RenderWindow window;

	// Event
	sf::Event event;
};
