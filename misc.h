/////////////////////////////////////////////////////////////////////////////////
// Misc
// Contains extra stuff, like includes and the pos struct
////////////////////////////////////////////////////////////////////////////////
#include "SFML/Graphics.hpp"

struct pos {
	pos(double a = 0, double b = 0) : x(a), y(b) {}
	bool operator==(const pos& rhs) {
		return (this->x == rhs.x ? true : false && this->y == rhs.y ? true : false);
	}
	pos& operator=(const pos& rhs) {
		this->x = rhs.x;
		this->y = rhs.y;
		return *this;
	}
	pos& operator+=(const double rhs) {
		this->x += rhs;
		this->y += rhs;
		return *this;
	}
	pos& operator+=(const pos& rhs) {
		this->x += rhs.x;
		this->y += rhs.y;
		return *this;
	}
	double x;
	double y;
};


