#include "game.h"

// Game Constants
// These can be tweaked by user, if they so wish
pos WINDOW_SIZE = { 1080, 720 };
pos BALL_SPEED = { 0.2, 0.2 };
pos BALL_START = { WINDOW_SIZE.x/2, WINDOW_SIZE.y/2 }; // Middle of the screen
sf::Color BALL_COLOR = sf::Color::White;
int BALL_RADIUS = 15;
pos PADDLE_SIZE = { 80, 8 };
pos USER_START = { 540, 670 };
pos AI_START = { 540, 50 };
pos USER_SPEED = { 1, 0 };
pos AI_SPEED = { 0.2, 0 };
std::string GAME_TITLE = "PONG!";

int main()
{
	Game game(WINDOW_SIZE.x, WINDOW_SIZE.y, BALL_SPEED, BALL_START, 
		BALL_COLOR, BALL_RADIUS, PADDLE_SIZE, USER_START, AI_START, 
		USER_SPEED, AI_SPEED, GAME_TITLE);
}